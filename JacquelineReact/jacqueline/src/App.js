import React from "react";
import "./App.css";
import Header from "./components/ui/header/header";
import Carousel from "./components/pages/home/carousel/carousel";
import Welcome from "./components/pages/home/welcome/welcome";
import Massage from "./components/pages/home/massage/massage";
import Brands from "./components/pages/home/Brands/brands";
import OpenHours from "./components/pages/home/openHours/openHours";
import News from "./components/pages/home/news/news";
import Services from "./components/pages/home/ServicesComponent/services";
import Products from "./components/pages/home/products/products";
import Appointment from "./components/pages/home/Appointment/appointment";
import Footer from "./components/ui/footer/footer";

function App() {
  return (
    <div className="App">
      <Header />
      <Carousel />
      <Welcome />
      <Massage />
      <Brands />
      <OpenHours />
      <News />
      <Services />
      <Products />
      <Appointment />
      <Footer />
    </div>
  );
}

export default App;

import React from "react";

const ProductsItem = () => {
  return (
    <div className="col-12 col-lg-3 col-md-4 col-sm-6">
      <div className="products-item">
        <div className="products-item-img">
          <img
            src="http://jacqueline-html.themerex.net/images/3473311504060-hd.jpg"
            alt =""
          />
          <div className="products-item-img-overplay">
            <div className="products-item-img-overplay-icon">
              <i className="icon-left fas fa-search " />
              <i className="icon-right fas fa-arrow-right " />
            </div>
          </div>
        </div>
        <p>
          <span>SIMPLE</span> , <span>SKINCARE</span>
        </p>
        <h5>THE SUPREME SKIN CARE</h5>
        <h4>£47.00</h4>
      </div>
    </div>
  );
};

export default ProductsItem;

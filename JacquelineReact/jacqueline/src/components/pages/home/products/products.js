import React from 'react'
import ProductsItem from './productsItem';

const Products=()=> {
    return (
      <section className="products">
        <div className="container">
          <div className="products-title text-center">
            <h4 className="main-title-font-Haviland">From The Shop</h4>
            <h2 className="main-title">OUR PRODUCTS</h2>
          </div>
          <div className="products-contain">
            <div className="row">
              <ProductsItem />
              <div className="col-12 col-lg-3 col-md-4 col-sm-6">
                <div className="products-item">
                  <div className="products-item-img">
                    <img
                      src="http://jacqueline-html.themerex.net/images/3473311421005_95.jpg"
                      alt =""
                    />
                    <div className="products-item-img-overplay">
                      <div className="products-item-img-overplay-icon">
                        <i className="icon-left fas fa-search " />
                        <i className="icon-right fas fa-arrow-right " />
                      </div>
                    </div>
                  </div>
                  <p>
                    <span>SIMPLE</span> , <span>SKINCARE</span>
                  </p>
                  <h5>MASQUE CONTOUR DES YEUX</h5>
                  <h4>£22.00</h4>
                </div>
              </div>
              <div className="col-12 col-lg-3 col-md-4 col-sm-6">
                <div className="products-item">
                  <div className="products-item-img">
                    <img
                      src="http://jacqueline-html.themerex.net/images/3473311970015_94.jpg"
                      alt =""
                    />
                    <div className="products-item-img-overplay">
                      <div className="products-item-img-overplay-icon">
                        <i className="icon-left fas fa-search " />
                        <i className="icon-right fas fa-arrow-right " />
                      </div>
                    </div>
                  </div>
                  <p>
                    <span>SIMPLE</span> , <span>SKINCARE</span>
                  </p>
                  <h5>MASQUE CONTOUR DES YEUX</h5>
                  <h4>£22.00</h4>
                </div>
              </div>
              <div className="col-12 col-lg-3 col-md-4 col-sm-6">
                <div className="products-item">
                  <div className="products-item-img">
                    <img
                      src="http://jacqueline-html.themerex.net/images/3473311970015_94.jpg"
                      alt =""
                    />
                    <div className="products-item-img-overplay">
                      <div className="products-item-img-overplay-icon">
                        <i className="icon-left fas fa-search " />
                        <i className="icon-right fas fa-arrow-right " />
                      </div>
                    </div>
                  </div>
                  <p>
                    <span>SIMPLE</span> , <span>SKINCARE</span>
                  </p>
                  <h5>MASQUE CONTOUR DES YEUX</h5>
                  <h4>£22.00</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
}

export default Products

import React from 'react'
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import "../../../../styles/Helper/_customize.scss";
import Slider from "react-slick";
const Brands=()=> {
    const settings = {
        // dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
      //   autoplay: true,
        speed: 1000,
        autoplaySpeed: 5000,
        cssEase: "linear",
      };
    return (
        <section className="brands">
            <div className="container">
                <div className="row">
                    <div className="col-12 col-lg-4">
                        <div className="brands-left d-flex align-items-center ">
                            <h4> We use products of the best brands only</h4>
                        </div>
                    </div>
                    <div className="col-12 col-lg-8">
                        <div className="brands-right">
                            <Slider {...settings} className="mx-0">
                                    <div className="brands-right-item">
                                        <img src="http://jacqueline-html.themerex.net/images/logo-2.png" alt="" />
                                    </div>
                                    <div className="brands-right-item">
                                        <img src="http://jacqueline-html.themerex.net/images/logo-1.png" alt="" />
                                    </div>
                                    <div className="brands-right-item">
                                        <img src="http://jacqueline-html.themerex.net/images/logo-4.png" alt="" />
                                    </div>
                                    <div className="brands-right-item">
                                        <img src="http://jacqueline-html.themerex.net/images/logo-4.png" alt="" />
                                    </div>
                            </Slider>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}

export default Brands

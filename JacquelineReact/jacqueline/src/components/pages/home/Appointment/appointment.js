import React from 'react'

const Appointment=()=> {
    return (
      <section className="appointment d-flex justify-content-center align-items-center">
        <div className="appointment-content text-center">
          <h4>Our Special Offer</h4>
          <h3>YOU OWE YOURSELF THIS MOMENT</h3>
          <p>Visit one of our multiple sessions of relaxation.</p>
          <div className="appointment-Btn d-flex justify-content-center my-4">
            <button className="Btn-Special Btn-Pink d-flex -justify-content-center align-items-center">
              <div className="Btn-Special-contain">
                <span>MAKE AN APPOINTMENT</span>
                <span>MAKE AN APPOINTMENT</span>
              </div>
            </button>
          </div>
        </div>
      </section>
    );
}

export default Appointment

import React from "react";
import WelcomeItem from "./welcomeItem";

const Welcome = () => {
  const welcomeArr=[
    {
      img:"http://jacqueline-html.themerex.net/images/3-1-358x393.jpg",
    name:"Deal Of Week",
    describe:"SPECIAL OFFERS"},
    {img:"http://jacqueline-html.themerex.net/images/3-1-358x393.jpg",
    name:"Deal Of Month",
    describe:"SPECIAL OFFERS"},
    {img:"http://jacqueline-html.themerex.net/images/3-1-358x393.jpg",
    name:"Deal Of Year",
    describe:"SPECIAL OFFERS"},
  ]
  return (
    <section className="welcome">
      <div className="container">
        <div className="welcome-title">
          <h4 className="main-title-font-Haviland">Welcome to our</h4>
          <h2 className="main-title">MASSAGE THERAPHY CENTER</h2>
          <p>
            You deserve better than a rushed massage by a rookie therapist in
            <br />a place that makes you feel more stressed
          </p>
        </div>
        <div className="welcome-intro">
          <div className="row">
            {welcomeArr.map((item,index)=>(<WelcomeItem item={item} key={index}/>))}
            
            {/* <WelcomeItem />
            <WelcomeItem /> */}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Welcome;

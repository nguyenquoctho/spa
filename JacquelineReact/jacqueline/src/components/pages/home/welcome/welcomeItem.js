import React from 'react'

const WelcomeItem=({item,...props})=> {
    return (
      <>
        <div className="welcome-intro-item-col col-12 col-lg-4 col-md-6 p-0">
        <div className="welcome-intro-item">
          <div className="welcome-intro-item-img">
            <img
              src={item.img}
              alt=""
            />
            <div className="welcome-intro-item-img-overplay" />
          </div>
          <div className="welcome-intro-item-content">
            <div className="welcome-intro-item-content-cover">
              <div className="welcome-intro-item-content-cover-title">
                <p>{item.name}</p>
                <h5>{item.describe}</h5>
              </div>
              <button className="Btn-Special Btn-Pink d-flex -justify-content-center align-items-center">
                <div className="Btn-Special-contain">
                  <span className="d-block">LEARN MORE</span>
                  <span>LEARN MORE</span>
                </div>
              </button>
            </div>
          </div>
        </div>
        </div>
      </>
    );
}

export default WelcomeItem

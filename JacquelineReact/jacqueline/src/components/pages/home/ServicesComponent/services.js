import React from 'react'

const Services=()=> {
    return (
      <section className="services">
        <div className="container">
          <div className="services-contain">
            <div className="services-detail">
              <h3>SERVICES &amp; PRICING</h3>
              <p>
                At our massage theraphy center, each massage is customized
                according to the client's individual needs as well as the
                therapist's own talents and training.
              </p>
              <div className="row">
                <div className="col-6">
                  <span>Personalized Massage</span>
                </div>
                <div className="col-6 text-right">
                  <span>60 min $100 / 90 min $145</span>
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-6">
                  <span>Couples Massage</span>
                </div>
                <div className="col-6 text-right">
                  <span>60 min $110 per person</span>
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-6">
                  <span>Hot Stone Massage</span>
                </div>
                <div className="col-6 text-right">
                  <span>60 min $125 / 90 min $170</span>
                </div>
              </div>
              <button className="Btn-long">
                <div className="animation-long" />
                <span>BOOK ONLINE</span>
              </button>
            </div>
          </div>
        </div>
      </section>
    );
}

export default Services

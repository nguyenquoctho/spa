import React from 'react'
import NewsItem from './newsItem';

const News=()=> {
    return (
      <section className="news">
        <div className="container">
          <div className="news-title text-center">
            <h4 className="main-title-font-Haviland">From The Blog</h4>
            <h2 className="main-title">LATEST NEWS &amp; EVENTS</h2>
          </div>
          <div className="new-content">
            <div className="row">
                <NewsItem />
              <div className="col-12 col-lg-3 col-md-6">
                <div className="new-content-item">
                  <div className="new-content-item-img">
                    <img
                      src="http://jacqueline-html.themerex.net/images/image-18-370x370.jpg"
                      alt=""
                    />
                    <div className="new-content-item-img-overplay" />
                  </div>
                  <span className="new-content-item-hover new-content-item-pink">
                    NEWS
                  </span>
                  <h4 className="new-content-item-hover">
                    WHAT IS MASSAGE THERAPY?
                  </h4>
                  <p>
                    <span className="new-content-item-hover">
                      March 29, 2016{" "}
                    </span>
                    Posted by{" "}
                    <span className="new-content-item-pink">Kate Green</span>
                  </p>
                </div>
              </div>
              <div className="col-12 col-lg-3 col-md-6">
                <div className="new-content-item">
                  <div className="new-content-item-img">
                    <img
                      src="http://jacqueline-html.themerex.net/images/image-19-370x370.jpg"
                      alt=""
                    />
                    <div className="new-content-item-img-overplay" />
                  </div>
                  <span className="new-content-item-hover new-content-item-pink">
                    NEWS
                  </span>
                  <h4 className="new-content-item-hover">
                    HOW OFTEN SHOULD I GET A MASSAGE?
                  </h4>
                  <p>
                    <span className="new-content-item-hover">
                      March 29, 2016{" "}
                    </span>
                    Posted by{" "}
                    <span className="new-content-item-pink">Kate Green</span>
                  </p>
                </div>
              </div>
              <div className="col-12 col-lg-3 col-md-6">
                <div className="new-content-item">
                  <div className="new-content-item-img">
                    <img
                      src="http://jacqueline-html.themerex.net/images/image-3-370x370.jpg"
                      alt=""
                    />
                    <div className="new-content-item-img-overplay" />
                  </div>
                  <span className="new-content-item-hover new-content-item-pink">
                    NEWS
                  </span>
                  <h4 className="new-content-item-hover">
                    MASSAGE THERAPY FOR TIGHT MUSCLES
                  </h4>
                  <p>
                    <span className="new-content-item-hover">
                      March 29, 2016{" "}
                    </span>
                    Posted by{" "}
                    <span className="new-content-item-pink">Kate Green</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="news-Btn d-flex justify-content-center my-4">
            <button className="Btn-Special Btn-Pink d-flex -justify-content-center align-items-center">
              <div className="Btn-Special-contain">
                <span className="d-block">VIEW ALL POSTS</span>
                <span>VIEW ALL POSTS</span>
              </div>
            </button>
          </div>
        </div>
      </section>
    );
}

export default News

import React from 'react'

const NewsItem=()=> {
    return (
        <div className="col-12 col-lg-3 col-md-6">
        <div className="new-content-item">
          <div className="new-content-item-img">
            <img
              src="http://jacqueline-html.themerex.net/images/image-3-370x370.jpg"
              alt=""
            />
            <div className="new-content-item-img-overplay" />
          </div>
          <span className="new-content-item-hover new-content-item-pink">
            NEWS
          </span>
          <h4 className="new-content-item-hover">
            HOW MASSAGE LOSES ITS VALUE
          </h4>
          <p>
            <span className="new-content-item-hover">
              March 29, 2016{" "}
            </span>
            Posted by{" "}
            <span className="new-content-item-pink">Kate Green</span>
          </p>
        </div>
      </div>
    )
}

export default NewsItem

import React from 'react'

const OpenHours=()=> {
    return (
        <section className="openHours">
            <div className="container">
                <div className="row">
                    <div className="col-12 col-xl-4">
                        <div className="openHours-left">
                            <h3>
                                OPEN HOURS:</h3>
                            <p>If you feel tired and stressed after a working day, we are happy to give you an enjoyable and healthy solution to find your balance again.</p>
                            <ul>
                                <li>Mon-Fri: 9 AM – 6 PM</li>
                                <li>Saturday: 9 AM – 4 PM</li>
                                <li>Sunday: Closed</li>
                            </ul>
                            <button className="Btn-long Btn-long-pink">
                                <div className="animation-long animation-long-pink" />
                                <span>BOOK ONLINE</span>
                            </button>
                        </div>
                    </div>
                    <div className="col-12 col-xl-8">
                        <div className="openHours-right">
                            <div className="openHours-right-img">
                                <img src="http://jacqueline-html.themerex.net/images/64695045.jpg" alt="" />
                            </div>
                            <div className="openHours-right-item">
                                <div className="openHours-right-item-content">
                                    <h3>01</h3>
                                    <p>
                                        July 4, 2015 / 0 Comments</p>
                                    <h2>MASSAGE<br />
                THERAPY<br />
                FOR REST &amp;<br />
                RELAXATION</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}

export default OpenHours

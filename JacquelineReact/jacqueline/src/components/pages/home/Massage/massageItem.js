import React from "react";

const MassageItem=()=> {
  return (
    <div className="col-12 col-lg-6 col-md-6 col-sm-6 px-0 m-0">
      <div className="massage-item">
        <i className="fas fa-bed" />
        <h3>MASSAGE THERAPY</h3>
        <p>
          We use different types of massage for a variety of health-related
          purposes
        </p>
      </div>
    </div>
  );
}

export default MassageItem;

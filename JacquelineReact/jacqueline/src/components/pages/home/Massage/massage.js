import React from "react";
import MassageItem from "./massageItem";

const Massage=()=> {
  return (
    <section className="massage">
      <div className="row m-0">
        <div className="col-12 col-xl-6 pr-0 pl-0">
          <div className="massage-left" />
        </div>
        <div className="col-12 col-xl-6">
          <div className="massage-right">
            <div className="row">
              <MassageItem />
              <div className="col-12 col-lg-6 col-md-6 col-sm-6 px-0 m-0">
                <div className="massage-item">
                  <i className="fas fa-bed" />
                  <h3>PHYSIOTHERAPY</h3>
                  <p>
                    It helps restore movement and function when someone is
                    affected by injury
                  </p>
                </div>
              </div>
              <div className="col-12 col-lg-6 col-md-6 col-sm-6 px-0 m-0">
                <div className="massage-item">
                  <i className="fas fa-bed" />
                  <h3>HOT STONE MASSAGE</h3>
                  <p>
                    The therapist uses smooth, heated stones by placing them on
                    the body
                  </p>
                </div>
              </div>
              <div className="col-12 col-lg-6 col-md-6 col-sm-6 px-0 m-0">
                <div className="massage-item">
                  <i className="fas fa-bed" />
                  <h3>SPORTS MASSAGE</h3>
                  <p>
                    Provides innovative bodywork and geared toward the needs of
                    athletics
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Massage;

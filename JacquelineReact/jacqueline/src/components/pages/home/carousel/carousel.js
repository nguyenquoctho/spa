import React from 'react'
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import "../../../../styles/Helper/_customize.scss";
import Slider from "react-slick";
import CarouselItem from './carouselItem';


const Carousel=()=> {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      speed: 1000,
      autoplaySpeed: 5000,
      cssEase: "linear",
    };
    return (
        <section>
              <Slider {...settings} >
                  <CarouselItem />
                  <CarouselItem />
                  <CarouselItem />
              </Slider>
        </section>
    )
}

export default Carousel

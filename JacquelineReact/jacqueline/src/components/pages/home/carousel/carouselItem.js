import React from 'react'

const CarouselItem=()=> {
    return (
      <section className="carousel">
        <div className="carousel-content">
          <img
            src="http://jacqueline-html.themerex.net/images/Vector-Smart-Object-6.png"
            alt = ""
          />
          <h3>Welcome to our</h3>
          <h2>massage theraphy center </h2>
          <h5>
            Give yourself a moment to relax. Find a minute to rejuvenate your
            body{" "}
          </h5>
          <button className="btn-pink-basic">Purchase Template</button>
        </div>
      </section>
    );
}

export default CarouselItem

import React from 'react'

const Footer=()=> {
    return (
      <footer>
        <div className="container">
          <div className="footer-back-to-top">
            <div className="back-to-top">
              <div className="back-to-top-container">
                <a className="cd-top text-replace js-cd-top" href="#">
                  <i className="fas fa-chevron-up" />
                </a>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-xl-4 col-md-6">
              <div className="footer-item">
                <h4>ABOUT US</h4>
                <p>
                  Come in and our therapists provide the perfect treatment. Our
                  massage is specially designed to help you achieve the perfect
                  mind-body harmony
                </p>
              </div>
            </div>
            <div className="col-12 col-xl-4 col-md-6">
              <div className="footer-item">
                <h4>OPEN HOURS</h4>
                <p>Mon-Fri: 9 AM – 6 PM</p>
                <p>Saturday: 9 AM – 4 PM</p>
                <p>Sunday: Closed</p>
              </div>
            </div>
            <div className="col-12 col-xl-4 col-md-6">
              <div className="footer-item">
                <h4>CONTACTS</h4>
                <p>176 W street name, New York, NY 10014</p>
                <p>
                  Email: <a href="#">info@yoursite.com</a>
                </p>
                <p>
                  Telephone: <a href="#">+1(800)123-4566</a>
                </p>
              </div>
            </div>
          </div>
          <hr />
          <div className="newsletter">
            <h3>NEWSLETTER</h3>
            <div className="newsletter-subscribe d-flex justify-content-center align-items-center">
              <input
                className="pl-4"
                type="text"
                placeholder="Please, enter your email address"
              />
              <div className="footer-Btn  d-flex justify-content-center">
                <button className="Btn-Special Btn-Pink d-flex -justify-content-center align-items-center">
                  <div className="Btn-Special-contain">
                    <span className="d-block">SUBSCRIBE</span>
                    <span>SUBSCRIBE</span>
                  </div>
                </button>
              </div>
            </div>
            <div className="newsletter-icon">
              <i className="fab fa-twitter" />{" "}
              <i className="fab fa-facebook-f" />{" "}
              <i className="fab fa-instagram" />
            </div>
            <p>
              <span>Jacqueline</span> © 2019 All Rights Reserved{" "}
              <span> Terms of Use </span> and <span> Privacy Policy </span>
            </p>
          </div>
        </div>
      </footer>
    );
}

export default Footer

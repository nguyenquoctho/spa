import React from "react";

const Header = () => {
  return (
    <header>
      <div className="header-contact-content">
        <div className="container">
          <div className="header-contact d-flex justify-content-between align-items-center">
            <div className="header-contact-left">
              <span>Contact Us: info@yoursite.com</span>
              <span>
                Call Us <a href="#"> +1 800 245 39 25</a>
              </span>
            </div>
            <div className="header-contact-right">
              <span>Stay connected: </span>
              <a href="#">
                <i className="fab fa-twitter" />
              </a>
              <a href="#">
                {" "}
                <i className="fab fa-facebook-f" />
              </a>
              <a href="#">
                {" "}
                <i className="fab fa-instagram" />
              </a>
              <a className="contact-icon" href="#">
                {" "}
                <i className="fas fa-dice-one" />
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="navbar navbar-expand-md navbar-light">
        <div className="container">
          <button
            className="navbar-toggler"
            data-toggle="collapse"
            data-target="#myMenu"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <a
            href="#"
            className="navbar-brand
  navbar-brand--logo text-center"
          >
            <img
              src="http://jacqueline-html.themerex.net/images/Logo.png"
              alt=""
            />
          </a>
          <div className="collapse navbar-collapse" id="myMenu">
            <ul
              className="navbar-nav
      navbar-nav--menu ml-auto mb-2"
            >
              <li className="nav-item active">
                <a href="#" className="nav-link">
                  HOME
                </a>
                <div className="nav-item__line" />
              </li>
              <li className="nav-item">
                <a href="#" className="nav-link">
                  FEATURES
                </a>
                <div className="nav-item__line" />
              </li>
              <li className="nav-item">
                <a href="#" className="nav-link">
                  BOOKING
                </a>
                <div className="nav-item__line" />
              </li>
              <li className="nav-item">
                <a href="#" className="nav-link">
                  BLOG
                </a>
                <div className="nav-item__line" />
              </li>
              <li className="nav-item">
                <a href="#" className="nav-link">
                  SHOP
                </a>
                <div className="nav-item__line" />
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
